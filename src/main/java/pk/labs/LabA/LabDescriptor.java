package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "paczka.Wyswietlacz";
    public static String controlPanelImplClassName = "paczka.PanelKontrolny";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.Kuchenka";
    public static String mainComponentImplClassName = "paczka.Mikrofalowka";
    // endregion

    // region P2
    public static String mainComponentBeanName = "okno";
    public static String mainFrameBeanName = "app-frame";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "sf";
    // endregion
}
